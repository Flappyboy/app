const fs = require("fs");
const path = require("path")
const execSync = require('child_process').execSync;
const deleteFolderRecursive = (url) => {
  var files = [];
  /**
   * 判断给定的路径是否存在
   */
  if (fs.existsSync(url)) {
    /**
     * 返回文件和子目录的数组
     */
    files = fs.readdirSync(url);
    files.forEach(function (file, index) {

      var curPath = path.join(url, file);
      /**
       * fs.statSync同步读取文件夹文件，如果是文件夹，在重复触发函数
       */
      if (fs.statSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);

      } else {
        fs.unlinkSync(curPath);
      }
    });
    /**
     * 清除文件夹
     */
    fs.rmdirSync(url);
  } else {
    console.log("给定的路径不存在 "+url);
  }
}
const git_push = (remotes)=>{
  deleteFolderRecursive("./dist/.git")
  const add_remotes = []
  const pushs = []
  for (let i = 0; i < remotes.length; i++) {
    const {
      url,
      branch,
    } = remotes[i]
    add_remotes.push(`git remote add origin_${i} ${url}`)
    pushs.push(`git push -f origin_${i} master:${branch}`)
  }
  const cmds = `cd dist && git init && ${add_remotes.join(" && ")} ` +
    "&& git add . " +
    "&& git commit -m 'pages' " +
    `&& ${pushs.join(" && ")}`
  execSync(cmds)
  deleteFolderRecursive("./dist/.git")
  return cmds
}
module.exports = {
  deleteFolderRecursive,
  git_push
}
